# Contribute to mTHMMY

Thank you for your interest in contributing to mTHMMY! This guide details how
to contribute to mTHMMY in a way that is efficient for everyone.

## Security vulnerability disclosure

**Important!** Instead of creating publicly viewable issues for suspected security
vulnerabilities, please report them in private to
`thmmynolife@gmail.com`.

## I want to contribute!

There are many ways of contributing to mTHMMY:

- Simply using the [latest release version][google-play] from Google Play (anonymous reports are sent automatically)
- Joining our [Discord server][discord-server]
- Submitting bugs and ideas on our [Trello board][trello-board]
- Forking mTHMMY and submitting [merge requests](#merge-requests)
- Joining our core team
- Contacting us by email at `thmmynolife@gmail.com`

## Issue tracker

The [mTHMMY Board on trello.com][trello-board] is used as an Issue Tracker, for bugs and improvements concerning the available mTHMMY releases.
Before creating a card to submit an issue please **search the board** for similar entries.

## Merge requests

Merge requests with fixes and improvements to mTHMMY are most welcome. Any developer that wants to work independently from the core team can simply
follow the workflow below to make a merge request:

1. Fork the project into your personal space on GitLab.com
1. Create a feature branch, away from `develop`
1. Push the commit(s) to your fork
1. Create a merge request (MR) targeting `develop` [at mTHMMY](https://gitlab.com/ThmmyNoLife/mTHMMY/tree/develop)
1. Fill the MR title describing the change you want to make
1. Fill the MR description with a brief motive for your change and the method you used to achieve it
1. Submit the MR.



[google-play]: https://play.google.com/store/apps/details?id=gr.thmmy.mthmmy
[trello-board]: https://trello.com/invite/b/4MVlkrkg/44a931707bd0b84a5e0bdfc42b9ae4f1/mthmmy
[discord-server]: https://discord.gg/CVt3yrn
