# mTHMMY

[![build status](https://gitlab.com/ThmmyNoLife/mTHMMY/badges/develop/build.svg)](https://gitlab.com/ThmmyNoLife/mTHMMY/commits/develop)
[![API](https://img.shields.io/badge/API-19%2B-blue.svg?style=flat)](https://android-arsenal.com/api?level=19)
[![Discord Channel](https://img.shields.io/badge/discord-public@mTHMMY-738bd7.svg?style=flat)][discord-server]
[![Trello Board](https://img.shields.io/badge/trello-mTHMMY-red.svg?style=flat)][trello-board]


mTHMMY is a mobile app for the [thmmy.gr](https://www.thmmy.gr) community.

## Requirements

mTHMMY can be installed on any smartphone with Android 4.4 KitKat or newer.

## Download

The latest release version is available on Google Play:

<a href='https://play.google.com/store/apps/details?id=gr.thmmy.mthmmy&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' width="200"/></a>

## Contributing

Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

## Contact

Do not hesitate to contact us for any matter, either by sending an email to `thmmynolife@gmail.com`, or by joining our [Discord server][discord-server].

**Legal attribution: Google Play and the Google Play logo are trademarks of Google Inc.*

[discord-server]: https://discord.gg/CVt3yrn
[trello-board]: https://trello.com/invite/b/4MVlkrkg/44a931707bd0b84a5e0bdfc42b9ae4f1/mthmmy
